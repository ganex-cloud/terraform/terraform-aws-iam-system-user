variable "name" {
  description = "The Name of the user"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit','XYZ')`)"
}

variable "force_destroy" {
  description = "Destroy even if it has non-Terraform-managed IAM access keys, login profile or MFA devices."
  default     = "false"
}

variable "path" {
  description = "Path in which to create the user"
  default     = "/"
}

variable "policy" {
  type        = "string"
  description = "The policy document. This is a JSON formatted string."
  default     = ""
}

variable "iam_policies" {
  type        = "list"
  description = "list of ARN IAM policies to attach to the list of users"
  default     = []
}
