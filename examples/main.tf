module "iam-USER" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-iam-system-user.git?ref=master"
  name   = "USER"
  policy = "${file("files/FILE.json")}"

  iam_policies = [
    "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
    "arn:aws:iam::aws:policy/IAMFullAccess",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/AmazonVPCFullAccess",
    "arn:aws:iam::aws:policy/AWSCodeBuildAdminAccess",
    "arn:aws:iam::aws:policy/AmazonRoute53FullAccess",
  ]
}
