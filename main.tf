resource "aws_iam_user" "default" {
  name          = "${var.name}"
  path          = "${var.path}"
  force_destroy = "${var.force_destroy}"
}

resource "aws_iam_access_key" "default" {
  user = "${aws_iam_user.default.name}"
}

resource "aws_iam_user_policy" "default" {
  count      = "${length(var.policy) > 0 ? 1 : 0}"
  name       = "${var.name}"
  user       = "${var.name}"
  policy     = "${var.policy}"
  depends_on = ["aws_iam_user.default"]
}

resource "aws_iam_user_policy_attachment" "default" {
  count      = "${length(var.iam_policies) > 0 ? length(var.iam_policies) : 0}"
  user       = "${var.name}"
  policy_arn = "${var.iam_policies[count.index]}"
  depends_on = ["aws_iam_user.default"]
}
